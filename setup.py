#!/usr/bin/env python
# Installs bbql.

import os
import sys
from distutils.core import setup

requirements = open('requirements.txt').readlines()


def get_version():
    return open('version.txt', 'r').read().strip()


def long_description():
    """Get the long description from the README"""
    return open(os.path.join(sys.path[0], 'README.md')).read()


setup(
    author='Erik van Zijst',
    author_email='erik.van.zijst@gmail.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
    ],
    description='A simple query DSL for application in Django.',
    keywords='django bitbucket query filter',
    license='MIT',
    long_description=long_description(),
    name='bbql',
    packages=['bbql'],
    scripts=['bbqlsh.py'],
    url='https://bitbucket.org/bitbucket/bbql',
    install_requires=requirements,
    version=get_version(),
)
