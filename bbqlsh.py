#!/usr/bin/env python
import atexit
import readline
import traceback

from django.db.models import Q

from bbql import parser
from bbql.orm import ORMTransformer


try:
    readline.read_history_file('.bbql_history')
except IOError:
    pass
atexit.register(readline.write_history_file, '.bbql_history')


class DebugTransformer(ORMTransformer):
    def __init__(self):
        super(DebugTransformer, self).__init__(None)

    def expr(self, exp):
        field, op, value = exp.tail

        q = Q(**{''.join((field, {'=': '',
                                  '>': '__gt',
                                  '>=': '__gte',
                                  '<=': '__lte',
                                  '<': '__lt',
                                  '!=': '',
                                  '!~': '__icontains',
                                  '~': '__icontains'}[op])): value})
        return ~q if '!' in op else q


print('Interactive BBQL parser.')
print('Type your query below and press enter.')
print('Example:')
print('  (milestone.name ~ "M2" AND (reporter.username = "evzijst" OR id < 10 OR assignee = NULL))')


while True:
    try:
        line = raw_input('BBQL> ')
        if not line:
            break

        tree = parser.parse(line)
        print(tree.pretty())
        print(DebugTransformer().transform(tree))
    except EOFError:
        break
    except:
        traceback.print_exc()
