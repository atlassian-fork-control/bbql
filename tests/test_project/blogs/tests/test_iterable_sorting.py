import six

from unittest import TestCase

from bbql import execute as runbbql

from blogs.models import AccessLogEntry, Bytes
from blogs.tests import take


class SortTest(TestCase):
    def setUp(self):
        self.logs = [
            AccessLogEntry('/pa/th', 200, lambda: Bytes(100, 1024)),
            AccessLogEntry('/path', 200, None),
            AccessLogEntry('/path', 404, Bytes(0, 1023)),
            AccessLogEntry(None, None, Bytes(None, 1)),
        ]

    def test_ascending(self):
        result = runbbql(self.logs, sortcol='status')
        six.assertCountEqual(self, take([3, 0, 1, 2], self.logs),
                             list(result))

        result = runbbql(self.logs, sortcol='bytes.out')
        six.assertCountEqual(self, take([1, 3, 2, 0], self.logs),
                             list(result))

    def test_descending(self):
        result = runbbql(self.logs, sortcol='-status')
        six.assertCountEqual(self, take([2, 1, 0, 3], self.logs),
                             list(result))
