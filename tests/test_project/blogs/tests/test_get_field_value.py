from bbql import get_field_value
from bbql.exceptions import InvalidPathException
from django.test import TestCase

from blogs.models import Blog


class TestGetField(TestCase):
    fixtures = ['test_data.json']

    def test_hybrid_orm_iterable(self):
        self.assertEqual(100, get_field_value(Blog.objects.get(pk=1),
                                              'traffic.in'))

    def test_unmapped_attribute(self):
        with self.assertRaises(InvalidPathException):
            get_field_value(Blog.objects.get(pk=1),
                                            'unfiltered_property')

    def test_empty_path(self):
        instance = Blog.objects.get(pk=1)
        self.assertEqual(instance, get_field_value(instance, ''))
