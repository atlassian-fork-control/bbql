import six

from bbql import execute as runbbql, filtered
from bbql.exceptions import (SortingException, InvalidSortStringException,
                             UnsortableFieldException)
from bbql.orm import QBuilder
from django.contrib.auth.models import User
from django.test import TestCase

from blogs.models import Comment


class SortTest(TestCase):
    fixtures = ['test_data.json']

    def setUp(self):
        self.erik = User.objects.get(username='erik')
        self.joe = User.objects.get(username='joe')

    def test_ascending(self):
        self.assertListEqual(
            list(Comment.objects.order_by('id')),
            list(runbbql(Comment.objects.all(), sortcol='id')))

    def test_descending(self):
        self.assertListEqual(
            list(Comment.objects.order_by('-id')),
            list(runbbql(Comment.objects.all(), sortcol='-id')))

    def test_custom_sorter(self):
        self.assertListEqual(
            list(Comment.objects.order_by('blog__status')),
            list(runbbql(Comment.objects.all(), sortcol='blog.state')))

    def test_nested_column(self):
        six.assertCountEqual(
            self,
            list(Comment.objects.order_by('blog__commenting_closed')),
            list(runbbql(Comment.objects.all(), sortcol='meta.muted'))
        )
        six.assertCountEqual(
            self,
            list(Comment.objects.order_by('-blog__commenting_closed')),
            list(runbbql(Comment.objects.all(), sortcol='-meta.muted'))
        )

    def test_unsortable_fields(self):
        with self.assertRaises(UnsortableFieldException):
            runbbql(Comment.objects.all(), sortcol='foo')

        with self.assertRaises(UnsortableFieldException):
            runbbql(Comment.objects.all(), sortcol='blog.foo')

        with self.assertRaises(UnsortableFieldException):
            runbbql(Comment.objects.all(), sortcol='content')

    def test_invalid_expressions(self):
        with self.assertRaises(SortingException):
            runbbql(Comment.objects.all(), sortcol='user.foo')

        with self.assertRaises(InvalidSortStringException):
            runbbql(Comment.objects.all(), sortcol=' id')

        with self.assertRaises(InvalidSortStringException):
            runbbql(Comment.objects.all(), sortcol='.id')

        with self.assertRaises(InvalidSortStringException):
            runbbql(Comment.objects.all(), sortcol='+id')

        with self.assertRaises(InvalidSortStringException):
            runbbql(Comment.objects.all(), sortcol='id.')

        with self.assertRaises(InvalidSortStringException):
            runbbql(Comment.objects.all(), sortcol='--id')

    def test_default_sorting(self):
        self.assertListEqual(
            list(Comment.objects.order_by('id')),
            list(filtered(
                defaultsort='id',
                query=lambda *args, **kwargs: None,
                sortcol=lambda *args, **kwargs: None)(Comment.objects.all)()))

    def test_default_sorting_reverse(self):
        self.assertListEqual(
            list(Comment.objects.order_by('-id')),
            list(filtered(
                defaultsort='-id',
                query=lambda *args, **kwargs: None,
                sortcol=lambda *args, **kwargs: None)(Comment.objects.all)()))

    def test_overriding_default_sorting(self):
        self.assertListEqual(
            list(Comment.objects.order_by('id')),
            list(filtered(
                defaultsort='-id',
                query=lambda *args, **kwargs: None,
                sortcol=lambda *args, **kwargs: 'id')(Comment.objects.all)()))

    def test_ignore_errors(self):
        """Assert that an invalid sort order column can be ignored."""
        expected = Comment.objects.order_by('id')
        result = QBuilder.registry[Comment].order_by(
            expected, '', ('foo.bar',), ignore_invalid_fields=True)

        self.assertListEqual(list(expected), list(result))
