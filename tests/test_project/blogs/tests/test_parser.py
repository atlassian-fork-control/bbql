from bbql import parser
from plyplus.plyplus import TokenizeError, ParseError
from unittest import TestCase

class ParserTests(TestCase):
    date_tests = [
        ('2013-05', False),
        ('2013-5-1', True),
        ('2013-05-01', True),
        ('2013-05-01T', False),
        ('2010-05-01T14:08', False),
        ('2010-05-01T14:08:59', True),
        ('2010-05-01T14:08:59.23423', True),
        ('2010-05-01T14:08:59.23423Z', True),
        ('2010-05-01T14:08:59.23423X', False),
        ('2010-05-01T14:08:59.23423+12:12', True),
        ('2010-05-01T14:08:59.23423 12:12', False),  # in case user forgets to %-encode the +
        ('2010-05-01T14:08:59.23423-12:12', True),
    ]

    def test_date_parsing(self):
        for d in self.date_tests:
            was_parsed = True
            try:
                parser.parse('username > %s' % d[0])
            except (ParseError, TokenizeError):
                was_parsed = False

            self.assertEqual(d[1], was_parsed,
                'Expected date %s to %sbe parsed' % (d[0],
                                                     '' if d[1] else 'not '))
