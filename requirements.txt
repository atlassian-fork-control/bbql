future>=0.17,<1
python-dateutil>=2.4.2,<3
# TODO - 0.7 moved some things around, requires slight refactor
PlyPlus>=0.6.2,<0.7.0
ply>=3.6,<4
wsgiref>=0.1.2;python_version<"3"
Django>=1.10,<2
