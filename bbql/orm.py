from functools import reduce
from itertools import chain

from django.db.models import Q
from django.db.models.fields.related import RelatedField
from plyplus import PlyplusException

from bbql.exceptions import (InvalidSortStringException,
                             UnsortableFieldException,
                             UnqueryableFieldException,
                             InvalidQueryStringException, InvalidPathException)
from bbql.compat import related_obj_types, get_related_obj_type
from bbql.parser import parser, BaseTransformer
from bbql.resolver import get_tokenized_field_value


def get_orm_field_value(item, field_toks):
    field, tail, column = QBuilder.registry[type(item)].match_column(field_toks)

    if isinstance(column, Column):
        item = column.get_value(item)
    elif column:
        item = reduce(getattr, column.split('__'), item)
    else:
        raise InvalidPathException()

    return get_tokenized_field_value(item, tail) if tail else item


def _resolve_model_field(path, model):
    """Given a tokenized Django ORM field (e.g.
    `'owner__profile__is_team'.split('__')`) and a Model class (e.g.
    `Repository`), resolves the field path down the model's related fields and
    returns the model class of the final segment.

    If the final segment of the field path is not a `RelatedField` or
    `RelatedObject`, but instead a primitive type (e.g. `CharField`), `None` is
    returned.

    :param tuple path:  a tokenized Django ORM field
    :param django.db.models.base.Model model:   the path's starting Model class
    :return django.db.models.base.Model:    the final segment's type
    """
    head, tail = path[0], path[1:]
    field = model._meta.get_field(head)
    if isinstance(field, RelatedField):
        model = field.rel.to
    elif isinstance(field, related_obj_types):
        model = get_related_obj_type(field)
    elif tail:
        raise ValueError('Cannot resolve column')
    else:
        return None

    return _resolve_model_field(tail, model) if tail else model


class QBuilder(object):
    registry = {}

    def __init__(self, model, fields):
        self.model = model
        self.fields = [(tuple(k.split('.')), v) for k, v in fields]

    def match_column(self, field_toks):
        """Matches the start of `field_toks` (the user's input) against the
        set of fields registered by the object filter.

        Note that object filters can map a sequence of more than one element
        segments to a column (e.g. `'content.raw': 'content'`).

        :param field_toks:  the tokenized field tuple (e.g. ('blog', 'user',
                                                             'id'))
        :return:    a 3-tuple containing the consumed field
                    (e.g. `content.raw`), the remaining tokens and the filter's
                    column definition.
        """
        for k, col in self.fields:
            if field_toks[:len(k)] == k:
                return k, field_toks[len(k):], col
        return None, None, None

    def q(self, field_toks, op, value, ormprefix, ignore_invalid_fields=False):
        """Creates a `Q` object for the given expression against a specific
        `Model` class.

        :param field_toks:  the tokenized field (e.g. ['blog', 'user', 'id'])
        :param op:      the operator (e.g. '=', '!=', '>=', etc)
        :param value:   the expression's left operand containing the value
        :param ormprefix:   the ORM field prefix (e.g. 'blog__user')
        :param ignore_invalid_fields:   don't raise `UnqueryableFieldException`,
                                        but silently ignore invalid fields
        """
        field, tail, column = self.match_column(field_toks)

        if isinstance(column, Column):
            return column.qs(field_toks, op, value, ormprefix)
        elif column:
            model = _resolve_model_field(column.split('__'), self.model)
            if model and tail:
                return self.registry[model].q(
                    tail, op, value, ormprefix + column + '__',
                    ignore_invalid_fields)
            elif not tail:
                return Q(**{ormprefix + column + op: value})

        if ignore_invalid_fields:
            return Q()
        else:
            raise UnqueryableFieldException(
                '.'.join(chain(ormprefix.split('__'), field_toks)))

    def order_by(self, qs, direction, field_toks, ormprefix=None,
                 ignore_invalid_fields=False):
        field, tail, column = self.match_column(field_toks)

        if isinstance(column, Column):
            return column.order_by(qs, direction, field_toks, ormprefix)
        elif column:
            model = _resolve_model_field(column.split('__'), self.model)
            if model and tail:
                return self.registry[model].order_by(
                    qs, direction, tail, ormprefix + column + '__',
                    ignore_invalid_fields)
            elif not tail:
                return qs.order_by(direction + ormprefix + column)

        if ignore_invalid_fields:
            return qs
        else:
            raise UnsortableFieldException(
                '.'.join(chain(ormprefix.split('__'), field_toks)))


class ORMTransformer(BaseTransformer):

    def __init__(self, model):
        self.model = model

    def expr(self, exp):
        field, op, value = exp.tail

        q = QBuilder.registry[self.model].q(
            tuple(field.split('.')),
            {'=': '',
             '~': '__icontains',
             '!=': '',     # special case, handled by ~Q() below
             '!~': '__icontains',
             '>': '__gt',
             '>=': '__gte',
             '<=': '__lte',
             '<': '__lt'}[op], value, '')
        return ~q if '!' in op else q

    def filter(self, qs, query):
        try:
            tree = parser.parse(query)
        except PlyplusException as e:
            raise InvalidQueryStringException(e)
        else:
            return qs.filter(self.transform(tree))

    def order_by(self, qs, exp):
        """Takes a `QuerySet` and a sort expression
        (e.g. `-blog.user.username`), resolves the expression to an ORM path
        and applies it to the queryset as `ORDER BY` clause.
        """
        m = self.sort_re.match(exp)
        if m:
            direction, field = m.groups()
            return QBuilder.registry[self.model].order_by(
                qs, direction, tuple(field.split('.')), '')
        raise InvalidSortStringException(exp)


def filters(model, fields):
    """Registers a query filter mapping for a Model `class`.

    This function can be used as a transparent decorator.

    :param model:   the Model class
    :param fields:  mapping of field names used in the query to the Model's
                    field names. Every queryable field must be explicitly
                    included
    """
    QBuilder.registry[model] = QBuilder(model, fields)
    return lambda func: func


class Column(object):
    def __init__(self, get_value=None, qs=None, order_by=None):
        self._get_value = get_value
        self._qs = qs
        self._order_by = order_by

    def get_value(self, instance):
        """Returns the value of this column/field for the given instance."""
        if self._get_value is not None:
            return self._get_value(instance)
        raise InvalidPathException()

    def qs(self, field_toks, op, value, ormprefix):
        if self._qs is not None:
            return self._qs(field_toks, op, value, ormprefix)
        raise UnqueryableFieldException(
            '.'.join(chain(ormprefix.split('__'), field_toks)))

    def order_by(self, qs, direction, field_toks, ormprefix):
        if self._order_by is not None:
            return self._order_by(qs, direction, field_toks, ormprefix)
        raise UnsortableFieldException(
            '.'.join(chain(ormprefix.split('__'), field_toks)))
