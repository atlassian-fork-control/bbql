import codecs
import re

from django.utils.module_loading import import_string
from future.utils import PY2
from dateutil.parser import parse as to_date
from plyplus import Grammar, STransformer

parser = Grammar("""
  @start: _or;

  ?_or: (_or '(?i)or')? _and;
  ?_and: (_and '(?i)and')? expr;

  ?expr: (field op value) | '\(' _or '\)';

  field: FIELD;

  FIELD: '[a-zA-Z_](?:\w+\.)*\w+'
  (%unless
  NULL: '(?i)null';
  AND: '(?i)and';
  OR: '(?i)or';
  TRUE: '(?i)true';
  FALSE: '(?i)false';
  );

  op: '=' | '>' | '>=' | '<=' | '<' | '!=' | '~' | '!~';
  string: '"(?:[^"\\\\]|\\\\.)*"';
  iso8601: '\d{4}-\d{1,2}-\d{1,2}(?:T\d\d:\d\d:\d\d(?:(?:\.\d{1,})?(?:[\+-]\d\d:\d\d|Z)?)?)?';
  number: '[-+]?[0-9]*\.?[0-9]+';
  null: '(?i)null';
  boolean: '(?i)true' | '(?i)false';
  ?value: string | number | null | boolean | iso8601;

  WS: '[ \t]+' (%ignore);
""")


class BaseTransformer(STransformer):
    epoch = to_date('1970-01-01T00:00:00+00:00')
    sort_re = re.compile(r'^(-?)(\w+(?:\.\w+)*)$')

    _or = lambda self, exp: exp.tail[0] | exp.tail[1]
    _and = lambda self, exp: exp.tail[0] & exp.tail[1]

    if PY2:
      string = lambda self, exp: exp.tail[0][1:-1].encode('utf-8').decode(
          'string_escape'
      ).decode('utf-8')
    else:
      string = lambda self, exp: codecs.escape_decode(
        bytes(exp.tail[0][1:-1], 'utf-8'))[0].decode('utf-8')
    number = (lambda self, exp:
              float(exp.tail[0]) if '.' in exp.tail[0] else int(exp.tail[0]))
    null = lambda self, exp: None
    boolean = lambda self, exp: exp.tail[0].lower() == 'true'
    iso8601 = lambda self, exp: to_date(exp.tail[0], default=self.epoch)
    __default__ = lambda self, exp: exp.tail[0]

    def filter(self, result, query):
        raise NotImplemented

    def order_by(self, result, exp):
        raise NotImplemented

    @staticmethod
    def newinstance(classname, *args):
        """Factory method for transformer instances. Use this instead of
        instantiating Transformer classes directly.

        Normally, transformers are instantiated by the `bbql.utils.execute`
        which calls this factory and looks up the fully qualified classname in
        Django settings. This allows applications to override the
        implementation used at runtime.
        """
        return import_string(classname)(*args)
