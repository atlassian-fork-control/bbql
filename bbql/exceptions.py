from django.utils.translation import ugettext as _


class BBQLException(Exception):
    pass


class InvalidPathException(BBQLException):
    def __unicode__(self):
        return _(u'Invalid field name: %(msg)s') % {'msg': self.message}


class FilterException(BBQLException):
    def __unicode__(self):
        return _(u'Filter query failed: %(msg)s') % {'msg': self.message}


class InvalidQueryStringException(FilterException):
    def __unicode__(self):
        return (_(u'Invalid filter query expression: %(exp)s') %
                {'exp': self.message})


class UnqueryableFieldException(FilterException):
    def __unicode__(self):
        return (_(u'Field "%(field)s" does not support filtering') %
                {'field': self.message})


class SortingException(BBQLException):
    def __unicode__(self):
        return (_(u'Sorting expression failed: %(msg)s') %
                {'msg': self.message})


class UnsortableFieldException(SortingException):
    def __unicode__(self):
        return (_(u'Field "%(field)s" does not support sorting') %
                {'field': self.message})


class InvalidSortStringException(SortingException):
    def __unicode__(self):
        return _(u'Invalid sort expression: %(exp)s') % {'exp': self.message}
