from functools import wraps

from bbql.utils import execute


def filtered(*args, **kwargs):
    """View decorator that applies the caller's filter query to the view's
    return value, which is expected to be a `QuerySet` or an iterable.

    :keyword query:     when specified, provides a callable that, given the
                        view function's arguments, returns the BBQL query. When
                        omitted, the query is sourced from the `?q=` query
                        parameter as per:
                        `lambda request, *a, **kw: request.GET.get('sort')`
    :keyword sortcol:   when specified, provides a callable that, given the
                        view function's arguments, returns the BBQL sort
                        expression. When omitted, the sort expression is
                        sourced from the `?sort=` query parameter as per:
                        `lambda request, *a, **kw: request.GET.get('sort')`
    :keyword defaultsort:   specifies any default ordering that must be applied
                            in cases where the user did not provide an explicit
                            sorting criterion (e.g. `-updated_on`).
                            When no default sort order is specified and
                            `sortcol()` did not provide a value, the API
                            handler's result is passed through as-is.
    :raises FilterException:    if the filter query was either syntactically or
                                semantically invalid
    :raises SortingException:   if the sort clause was either syntactically or
                                semantically invalid
    """
    defaultsort = None
    query = lambda request, *a, **kw: request.GET.get('q')
    sortcol = lambda request, *a, **kw: request.GET.get('sort')

    parameterized = len(args) != 1 or not callable(args[0]) or bool(kwargs)
    if parameterized:
        defaultsort = kwargs.get('defaultsort')
        query = kwargs.get('query', query)
        sortcol = kwargs.get('sortcol', sortcol)

    def _filtered(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            v = func(*args, **kwargs)
            q = query(*args, **kwargs)
            col = sortcol(*args, **kwargs) or defaultsort

            return execute(v, query=q, sortcol=col)

        return wrapper
    return _filtered if parameterized else _filtered(args[0])
